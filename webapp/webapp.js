/**
 * 
 */
(function(){
	var app = angular.module('cloud-detector', ['ngAnimate']);
	
	app.controller('InitTaskCtrl', ['$log', '$http',
	                                '$scope', '$timeout',
	                                function($log, $http, $scope,
	                                		$timeout){
		
		$scope.numberJobs = [];
		$scope.jobsSelected = 1;
		
		$http.get('http://localhost:5000/number_cpus').
		then(function(result){
			var maxNumberJobs = parseInt(result.data.cpus);
			for(var i=1; i <= maxNumberJobs; i++)
				$scope.numberJobs.push(i);
			$scope.jobsSelected = maxNumberJobs == 1 ? 1 : maxNumberJobs - 1;
		}, function(response){
			//When error
			$scope.step = 3;
			$timeout(render, 10);
		});
		
		this.setNumberJob = function(numberJob){
			$scope.jobsSelected = numberJob;
		}
		
		this.directory = '';
		var directory;
		this.saveToa = true;
		
		$scope.step = 0;
		
		var output_data = {};
		
		var render = function(){
			componentHandler.upgradeAllRegistered();
		}
		
		this.resetStatus = function(clearDirectory){
			$scope.step = 0;
			$timeout(render, 10);
			if(clearDirectory == true)
				$scope.directory = '';
		}
		
		this.sendTask = function(){
			if(this.directory != ''){
				directory = this.directory;
				var saveToa = this.saveToa ? 1 : 0;
				$log.log(saveToa);
				$http.post('http://localhost:5000/init_task',
						{'directory': this.directory,
						 'numJobs': $scope.jobsSelected,
						 'saveToa': saveToa}).
						then(function(result){
							output_data.taskId = result.data.taskId
							$scope.step = 1;
							$timeout(render, 10);
							$timeout(checkStatus, 1000 * 60);
						}, function(response){
							//When error
							$scope.step = 3;
							$timeout(render, 10);
						});
			}
			else
				alert("You haven't specified a directory yet");
		}
		
		var checkStatus = function(){
			$http.post('http://localhost:5000/check_status',
					{'taskId': output_data.taskId,
					 'directory': directory}).
					then(function(result){
						ready = result.data.ready
						if(ready == '0')
							$timeout(checkStatus, 1000 * 60);
						else{
							fileWasCreated = result.data.created;
							if(fileWasCreated == '0')
								$scope.step = 3;
							else
								$scope.step = 2;
							$timeout(render, 10);
						}
					});
			
		}
		
	}]);	
})();