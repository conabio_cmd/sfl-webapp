# Instalación del front-end del paquete Spatial Feature Learning

## Prerequisitos

Instalar Spatial Feature Learning [https://bitbucket.org/conabio_cmd/spatial-feature-learning](https://bitbucket.org/conabio_cmd/spatial-feature-learning)

Una vez instalado, se debe cambiar la rama a la que está apuntando Spatial Feature Learning:

```console
$ cd $SFLPATH
$ hg update RapidEye-Clouds
``` 

Instalar RabbitMQ.

Para OSX:

```console
$ brew update
$ brew install rabbitmq
```

Brew instala rabbitmq en el directorio `/usr/local/sbin` que por defecto no se agrega al PATH. Es necesario hacer:

```console
$ export PATH=$PATH:/usr/local/sbin
```

Es recomendable hacer este cambio permanente al agregarlo al .bash_profile o .profile.

Para cualquier otro sistema operativo, por favor siga las instrucciones de [https://www.rabbitmq.com/download.html](https://www.rabbitmq.com/download.html)

Instalar Node.js

```console
$ brew install node
```

Instalar Bower
```console
$ npm install -g bower
```

## Instalación

Ir al directorio donde queramos tener el código y clonar este repositorio:

```console
$ git clone https://dzenteno@bitbucket.org/conabio_cmd/sfl-webapp.git
# Cambiarnos de directorio
$ cd sfl-webapp
```
Descargar el modelo entrenado:

```console
$ wget https://s3.amazonaws.com/nubes-rapid-eye/model/best-model.zip
$ unzip best-model.zip && rm best-model.zip
```

Instalar el paquete:

```console
$ pip install -e sfl_webapp
```

Instalar las dependencias de javascript:
```console
$ cd webapp
$ bower install --save
```

## Ejecutar el Front-End
```console
$ sfl_server.py
```


