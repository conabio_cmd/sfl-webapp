'''
Created on Aug 4, 2015

@author: Daniel
'''
import setuptools
requirements = [
    'spatial-feature-learning',
    'Paste',
    'celery',
    'Flask',
    'Flask-Cors',
    'CherryPy']

setuptools.setup(name="sfl-webapp",
                 version="0.1a",
                 packages=setuptools.find_packages(),
                 install_requires=requirements,
                 description=('Front-end for Spatial Feature Learning'),
                 author="Ecoinformatica-CONABIO",
                 url="https://bitbucket.org/conabio_cmd/sfl-webapp",
                 scripts=['./sfl_server.py']
                 )
