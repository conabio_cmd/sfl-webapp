#!/usr/bin/env python
'''
Created on Jul 30, 2015

@author: Daniel
'''
from tasks import webapp
from paste.translogger import TransLogger
import cherrypy
import webbrowser
import os
import subprocess
from subprocess import CalledProcessError
import threading


class RabbitThread(threading.Thread):
    def run(self):
        error_message = ('Something went wrong when trying to '
                         'start rabbitmq-server. Make sure it is '
                         'accessible to this script')
        command = ['rabbitmq-server']
        try:
            _ = subprocess.check_output(command)
        except Exception:
            raise Exception(error_message)


class CeleryThread(threading.Thread):
    def run(self):
        error_message = ('Something went wrong when trying to '
                         'start celery. Make sure it is '
                         'accessible to this script')
        command = ['celery',
                   '-A',
                   'tasks',
                   'worker',
                   '--loglevel=info']
        try:
            _ = subprocess.check_output(command)
        except Exception:
            raise Exception(error_message)


def run_server():
    # Check rabbitstatus
    running = is_rabbit_running()
    if not running:
        rabbit_thread = RabbitThread()
        # rabbit_thread.daemon = True
        rabbit_thread.start()
    # start celery
    celery_thread = CeleryThread()
    celery_thread.start()
    app_logged = TransLogger(webapp)
    cherrypy.tree.graft(app_logged, '/')
    cherrypy.config.update({
        'engine.autoreload.on': True,
        'log.screen': True,
        'server.socket_port': 5000,
        'server.socket_host': '0.0.0.0',
        })
    cherrypy.engine.start()
    # index.html path
    current_dir = os.path.abspath(__file__)
    current_dir = os.path.dirname(current_dir)
    webapp_dir = os.path.join(current_dir, '..',
                              'webapp',
                              'index.html')
    webapp_dir = os.path.abspath(webapp_dir)
    # Launch web browser
    webbrowser.open('file://'+webapp_dir)
    cherrypy.engine.block()


def is_rabbit_running():
    command = ['rabbitmqctl',
               'status']
    error_message = ('Something went wrong when trying to '
                     'execute rabbitmqctl. Make sure it is '
                     'accessible to this script')
    try:
        _ = subprocess.check_output(command)
        running = True
    except OSError:
        raise Exception(error_message)
    except CalledProcessError as e:
        if e.returncode == 2:
            running = False
        else:
            raise Exception(error_message)
    return running

if __name__ == '__main__':
    run_server()
