'''
Created on Jul 29, 2015

@author: Daniel
'''
from celery.app.base import Celery
import subprocess
from flask.app import Flask
from flask.globals import request
from flask.json import jsonify
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
from multiprocessing import cpu_count
import os
import glob
from calendar import timegm
from time import gmtime

app = Celery('tasks',
             backend='amqp://guest@localhost//',
             broker='amqp://guest@localhost//')
webapp = Flask(__name__)
cors = CORS(webapp)
webapp.config['CORS_HEADERS'] = 'Content-Type'


def get_output_name(input_dir):
    search_pattern = os.path.join(input_dir, '*.tif')
    tif_files = glob.glob(search_pattern)
    img_file = None
    for tif_file in tif_files:
        if "_browse" in tif_file or "_udm" in tif_file:
            continue
        else:
            img_file = tif_file
            break
    output_image = os.path.splitext(img_file)[0]
    output_image += '.mask'
    return output_image


@app.task
def detect_clouds(input_dir, n_jobs, save_toa):
    output_image = get_output_name(input_dir)
    model_dir = os.path.abspath(__file__)
    model_dir = os.path.join(model_dir, '../../best-model')
    model_dir = os.path.abspath(model_dir)
    command = ['sfl.py',
               'madmex_processing',
               '--input_dir',
               input_dir,
               '--output_image',
               output_image,
               '--trained_model',
               model_dir,
               '--n_jobs',
               str(n_jobs)]
    if save_toa:
        command.append('--save_toa')
    print(' '.join(command))
    subprocess.check_call(command)


@webapp.route('/init_task', methods=['POST'])
@cross_origin()
def init_task():
    content = request.get_json()
    input_dir = content['directory']
    n_jobs = content['numJobs']
    save_toa = content['saveToa']
    save_toa = True if save_toa == 1 else False
    task = detect_clouds.delay(input_dir, n_jobs, save_toa)
    content.setdefault('taskId', task.task_id)
    return jsonify(content)


@webapp.route('/check_status', methods=['POST'])
@cross_origin()
def check_status():
    content = request.get_json()
    task_id = content['taskId']
    res = app.AsyncResult(task_id)
    content['ready'] = 1 if res.ready() else 0
    content['created'] = 0
    if res.ready():
        # Check that the file was created
        input_dir = content['directory']
        print(input_dir)
        output_mask = get_output_name(input_dir)
        if not os.path.isfile(output_mask):
            content['created'] = 0
        else:
            seconds_created = os.path.getctime(output_mask)
            seconds_current = timegm(gmtime())
            seconds_difference = abs(seconds_created - seconds_current)
            minutes_difference = seconds_difference / 60
            if minutes_difference > 10:
                content['created'] = 0
            else:
                content['created'] = 1
    return jsonify(content)


@webapp.route('/number_cpus', methods=['GET'])
@cross_origin()
def get_number_cpus():
    content = dict()
    content.setdefault('cpus', cpu_count())
    return jsonify(content)
